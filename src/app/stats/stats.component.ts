import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.css']
})
export class StatsComponent implements OnInit {

  constructor() { }

  ngOnInit() {

  }

  numero = '';
  list: number[] = [];
  valorMaximo = 0;
  valorMinimo = 0;
  valorMedio = 0;

  adicionarNumero() {
    this.list.push(Number(this.numero));
    this.numero = '';
    console.log(this.list);
    this.calcularStats()
  }

  calcularStats() {


    var sum = 0;
    var elements = this.list.length;

    for( var i = 0; i < elements; i++ ){
      sum += this.list[i];
    }

    this.valorMedio = elements ? Math.round(sum/elements * 100)/100 : 0;
    this.valorMaximo = elements ? Math.max.apply(null, this.list) : 0;
    this.valorMinimo = elements ? Math.min.apply(null, this.list) : 0;

    console.log(this.valorMaximo, this.valorMinimo, this.valorMedio, elements)

  }

  removerNumero(i) {
    this.list.splice(i,1);
    this.calcularStats();
  }

}
